Fully functional  generation of [Jde chleba a potká chleba s máslem](https://www.youtube.com/watch?v=AW0EKmcltjA) text.

Run in Python 3 with no special dependencies.
The script does not accept any parameters or arguments.
Print generated text of story on stdout.
