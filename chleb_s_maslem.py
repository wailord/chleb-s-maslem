arr = [("chleba", "chleba", "chlebe"),
       ("rohlík", "rohlík", "rohlíku"),
       ("houska", "housku", "housko"),
       ("dalamánek", "dalamánek", "dalamánku"),
       ("loupák", "loupák", "loupáku"),
       ("vánočka", "vánočku", "vánočko"),
       ("mazanec", "mazanec", "mazanče"),
       ("veka", "veku", "veko"),
       ("toust", "toust", "touste"),
       ("bulka", "bulku", "bulko"),
       ("pletýnka", "pletýnku", "pletýnko"),
       ("bageta", "bagetu", "bageto"),
       ("makovka", "makovku", "makovko"),
       ("šáteček", "šáteček", "šátečku"),
       ("buchta", "buchtu", "buchto"),
       ("závin", "závin", "závine"),
       ("koláč", "koláč", "koláči"),
       ("kobliha", "koblihu", "kobliho"),
       ("vdolek", "vdolek", "vdolku"),
       ("preclík", "preclík", "preclík"),
       ("Hořická trubička", "Hořickou trubičku", "Hořická trubičko"),
       ("knedlík", "knedlík", "knedlíku"),
       ("rolka", "rolku", "rolko"),
       ("croissant", "croissant", "croissante"),
       ("perník", "perník", "perníku"),
       ("tyčinka", "tyčinku", "tyčinko"),
       ("oválek", "oválek", "oválku"),
       ("taštička", "taštičku", "taštičko"),
       ("frgál", "frgál", "frgále"),
       ("raženka", "raženku", "raženko"),
       ("banketka", "banketku", "banketko"),
       ("hamburger", "haburger", "hamburgere"),
       ("pizza", "pizzu", "pizzo"),
       ("pirožek", "pirožek", "pirožku"),
       ("šnek", "šnek", "šneku"),
       ("placka", "placku", "placko"),
       ("bábovka", "bábovku", "bábovko"),
       ("muffin", "muffin", "muffine"),
       ("sušenka", "sušenku", "sušenko"),
       ("pusinka", "pusinku", "pusinko"),
       ("křupánek", "křupánek", "křupánku"),
       ("úlek", "úlek", "úlku"),
       ("indiánek", "indiánka", "indiánku"),
       ("bombička", "bombičku", "bombičko"),
       ("věneček", "věneček", "věnečku"),
       ("makovec", "makovec", "makovče"),
       ("medovník", "medovník", "medovníku"),
       ("punčák", "punčák", "punčáku"),
       ("dortík", "dortík", "dortíku"),
       ("větrník", "větrník", "větrníku"),
       ("bublanina", "bublaninu", "bublanino"),
       ("palačinka", "palačinku", "palačinko"),
       ("lívanec", "lívanec", "lívanče"),
       ("tiramisu", "tiramisu", "tiramisu"),
       ("cheesecake", "cheesecake", "cheesacak")]

arr = [y for y in arr for y in [(y, ""), (y, " s máslem"), (y, " s máslem se salámem")]]
print(arr)
arr.append((("rajče", "rajče", "rajče"), ""))

arrr = [[arr[j] for j in range(i + 1)] for i in range(len(arr))]

out = ["".join(
    ["Jde ", ", ".join(map(lambda x: x[0][0] + x[1], go)), " a potká ", ask[0][1] + ask[1], ". A ", ask[0][0] + ask[1],
     " povída \"",
     ", ".join(map(lambda x: x[1][0][2] + x[1][1] if x[0] else x[1][0][2].capitalize() + x[1][1], enumerate(go))),
     " můžu jít s vámi?\" A " if len(go) > 1 else " můžu jít s tebou?\" A ",
     ", ".join(map(lambda x: x[0][0] + x[1], go)),
     " odpoví. Jo můžeš." if len(arr) - 2 != i else " odpoví. Ne nesmíš."]) for i, (ask, go) in
    enumerate(zip(arr[1:], arrr))]

print("\n".join(out))
